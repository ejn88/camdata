#' MPS-MRP-DRP Lower Level Requirements File
#' 
#' F3412 - MPS-MRP-DRP Lower Level Requirements File 
#' 
#' @param db2_connection JDE Database Connection
#' @param friendly_names Flag to return human-readable column names.  Default TRUE.
#' @export
F3412 <- function(db2_connection, friendly_names = TRUE) {
    F3412 <- dplyr::tbl(db2_connection, dbplyr::in_schema('PRODDTA', 'F3412'))
        if (friendly_names) {
            F3412 <- F3412 %>% dplyr::select(
                `Item Number - Short` = `MWITM`,
                `Business Unit` = `MWMCU`,
                `Date - Requested` = `MWDRQJ`,
                `Leadtime Offset Days` = `MWLOVD`,
                `Parent (short) Item Number` = `MWKIT`,
                `Branch` = `MWMMCU`,
                `Units - Order/Transaction Quantity` = `MWUORG`,
                `Document (Order No Invoice etc.)` = `MWDOCO`,
                `Order Type` = `MWDCTO`,
                `Company - Key (Related Order)` = `MWRKCO`,
                `Related PO/SO/WO Number` = `MWRORN`,
                `Related PO/SO/WO Order Type` = `MWRCTO`,
                `Related PO/SO Line Number` = `MWRLLN`,
                `Unique Key ID (Internal)` = `MWUKID`,
                `Pegging Record Link (Internal)` = `MWPLNK`,
                `Project Number` = `MWPRJM`,
                `Source Demand for Replenishment` = `MWSRDM`,
                `Production Number Short` = `MWPNS`
            )
        }
    return(F3412)
}