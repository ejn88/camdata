#' Work Order Parts List
#' 
#' F3111 - Work Order Parts List 
#' 
#' @param db2_connection JDE Database Connection
#' @param friendly_names Flag to return human-readable column names.  Default TRUE.
#' @export
F3111 <- function(db2_connection, friendly_names = TRUE) {
    F3111 <- dplyr::tbl(db2_connection, dbplyr::in_schema('PRODDTA', 'F3111'))
        if (friendly_names) {
            F3111 <- F3111 %>% dplyr::select(
                `Document (Order No Invoice etc.)` = `WMDOCO`,
                `Order Type` = `WMDCTO`,
                `Order Suffix` = `WMSFXO`,
                `Type Bill of Material` = `WMTBM`,
                `Fixed or Variable Quantity` = `WMFORQ`,
                `Issue Type Code` = `WMITC`,
                `Co-Products/By-Products/Intermediate` = `WMCOBY`,
                `Component Type` = `WMCOTY`,
                `Component Line Number` = `WMCPNT`,
                `From Potency` = `WMFRMP`,
                `Thru Potency` = `WMTHRP`,
                `From Grade` = `WMFRGD`,
                `Thru Grade` = `WMTHGD`,
                `Company - Key (Related Order)` = `WMRKCO`,
                `Related PO/SO/WO Number` = `WMRORN`,
                `Related PO/SO/WO Order Type` = `WMRCTO`,
                `Related PO/SO Line Number` = `WMRLLN`,
                `Sequence Number - Operations` = `WMOPSQ`,
                `Sequence - Bubble Sequence` = `WMBSEQ`,
                `Resource Percent` = `WMRSCP`,
                `Percent - Percent of Scrap` = `WMSCRP`,
                `Percent - Rework` = `WMREWP`,
                `Percent - As is` = `WMASIP`,
                `Percent - Cumulative Planned Yield` = `WMCPYP`,
                `Operation Scrap Percent` = `WMSTPP`,
                `Leadtime Offset Days` = `WMLOVD`,
                `Component Item Number - Short` = `WMCPIT`,
                `Component 2nd Item Number` = `WMCPIL`,
                `Component 3rd Item Number` = `WMCPIA`,
                `Component Branch` = `WMCMCU`,
                `Description` = `WMDSC1`,
                `Description - Line 2` = `WMDSC2`,
                `Location` = `WMLOCN`,
                `Lot/Serial Number` = `WMLOTN`,
                `Address Number` = `WMAN8`,
                `Line Type` = `WMLNTY`,
                `Serial Number - Lot` = `WMSERN`,
                `Date - Order/Transaction` = `WMTRDJ`,
                `Date - Requested` = `WMDRQJ`,
                `Units - Order/Transaction Quantity` = `WMUORG`,
                `Quantity Available` = `WMTRQT`,
                `Units - Quantity Canceled/Scrapped` = `WMSOCN`,
                `Units - Qty Backordered/Held` = `WMSOBK`,
                `Hours - Unaccounted Direct Labor` = `WMCTS1`,
                `Units - Quantity Commited` = `WMQNTA`,
                `Unit of Measure` = `WMUM`,
                `Amount - Estimated Cost` = `WMEA`,
                `Route Sheet (N)` = `WMRTG`,
                `Material Status Code W.O.` = `WMMTST`,
                `Document Type` = `WMDCT`,
                `Message Number` = `WMSHNO`,
                `Business Unit` = `WMMCU`,
                `Project Business Unit` = `WMOMCU`,
                `Object Account` = `WMOBJ`,
                `Subsidiary` = `WMSUB`,
                `Component Revision Level` = `WMCMRV`,
                `Date - Completion (Julian)` = `WMSTRX`,
                `Number - Parent WO Number` = `WMPARS`,
                `Committed (H/S)` = `WMCOMM`,
                `User Reserved Code` = `WMURCD`,
                `User Reserved Date` = `WMURDT`,
                `User Reserved Amount` = `WMURAT`,
                `User Reserved Reference` = `WMURRF`,
                `User Reserved Number` = `WMURAB`,
                `User ID` = `WMUSER`,
                `Program ID` = `WMPID`,
                `Work Station ID` = `WMJOBN`,
                `Date - Updated` = `WMUPMJ`,
                `Time of Day` = `WMTDAY`,
                `Unique Key ID (Internal)` = `WMUKID`,
                `Primary / Last Supplier Number` = `WMVEND`,
                `Issue and Receipt` = `WMPOC`,
                `Amount - Unaccounted Direct Labor` = `WMCTS4`,
                `Amount - Unaccounted Scrap` = `WMCTS7`,
                `Units - Unaccounted Scrap` = `WMCTS8`,
                `G/L Date` = `WMGLD`,
                `Parts List Substitute Flag` = `WMSBFL`,
                `Active Ingredient Flag` = `WMAING`,
                `Secondary Quantity Issued/Completed` = `WMSSTQ`,
                `Unit of Measure - Secondary` = `WMUOM2`,
                `Constraints Flag` = `WMAPSC`,
                `Pick Slip Number` = `WMPSN`,
                `Lot Effectivity Date` = `WMDLEJ`,
                `Cost Component` = `WMCOST`,
                `Critical Hold Propagation` = `WMCHPP`,
                `Component Line Number BOM` = `WMCPNB`,
                `Bubble Sequence Alpha Numeric` = `WMBSEQAN`
            )
        }
    return(F3111)
}