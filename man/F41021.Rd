% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/F41021.R
\name{F41021}
\alias{F41021}
\title{Item Location File}
\usage{
F41021(db2_connection, friendly_names = TRUE)
}
\arguments{
\item{db2_connection}{JDE Database Connection}

\item{friendly_names}{Flag to return human-readable column names.  Default TRUE.}
}
\description{
F41021 - Item Location File
}
